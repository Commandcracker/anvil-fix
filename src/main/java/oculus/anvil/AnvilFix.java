package oculus.anvil;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.plugin.java.JavaPlugin;

public class AnvilFix extends JavaPlugin implements Listener {
    public void onEnable() {
        new MetricsLite(this, 9985);
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent evt) {
        if (!evt.isCancelled()) {
            if (evt.getPlayer().hasPermission("anvilfix.use")) {
                if (evt.getInventory().getType() == InventoryType.ANVIL) {
                    AnvilInventory inv = (AnvilInventory) evt.getInventory();
                    inv.setMaximumRepairCost(999999999);
                }
            }
        }
    }

}
